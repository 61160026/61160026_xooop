import java.util.Scanner;

public class Game {
	static Scanner kb = new Scanner(System.in);
	static Table table = new Table();
	static Game game = new Game();
	static char turn;
	static int col;
	static int row;
	static int playerXWin = 0;
	static int playerOWin = 0;
	
	public static void showWelcome() {
        System.out.println("***** Welcome to OX GAME *****"); 
        
    }
	
	public static void showTurn(char turn) {
        System.out.println("Turn : " + turn);
        
    }
	
	public static void showBye() {
        System.out.println("Bye Bye ,See you next time!!");
        
    }
	
	public static void showWin(char turn) {
        System.out.println("Player " + turn + " win!!");
        
    }
	
	public static void showDraw() {
        System.out.println("It is draw!!");
        
    }
	
	public static void showScoreO() {
		System.out.println("Score O : "+game.playerOWin);
        
    }
	
	public static void showScoreX() {
		System.out.println("Score X : "+game.playerXWin);
        
    }
	
	public static void Input(Table table, int count,char turn) { 
        System.out.println("Please input row and collum : ");
        row = kb.nextInt() - 1;
        col = kb.nextInt() - 1;
        addOX(table, count, turn);
    }
	
	public static void addOX(Table table, int count,char turn) {
		if (table.checkInputError(row, col) == true ||  table.checkRepeatError(table, row, col) == true) {
            Input(table, count,turn);
        }else {
        	Table.table[row][col] = turn;
        }
        
    }
	
	public static char changeTurn(char turn) {
        if (turn == 'O') {
            turn = 'X';
        } else {
            turn = 'O';
        }
        return turn;
    }
	
	public static void countWin(char turn) {
		if(turn == 'O') {
			playerOWin++;
		}else if(turn == 'X') {
			playerXWin++;
		}
	}
}
