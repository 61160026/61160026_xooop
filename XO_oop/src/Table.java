
public class Table {
	static char[][] table = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	static int row;
	static int col;
	
	public static void setRow(int row) {
		Table.row = row;
	}
	
	public static void setCol(int col) {
		Table.col = col;
	}
	
	public static void setTable(char[][] table) {
		Table.table = table;
	}
	
	public static void showTable() {
        for (int i = 0; i < 3; i++) {
            System.out.println("-------------");
            for (int j = 0; j < 3; j++) {
                System.out.print("| ");
                System.out.print(table[i][j] + " ");
            }
            System.out.print("|");
            System.out.println();
        }
        System.out.println("-------------");
    }
	
	public static void createNewTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = '-';
            }
        }
    }

	public static boolean checkInputError(int row, int col) {
        if ((row > 2 || row < 0) && (col > 2 || col < 0)) {
            System.out.println("ERROR Row & Collum!!, Please input 1 or 2 or 3");
            return true;
        } else if (col > 2 || col < 0) {
            System.out.println("ERROR Collum!!, Please input 1 or 2 or 3");
            return true;
        } else if (row > 2 || row < 0) {
            System.out.println("ERROR Row!!, Please input 1 or 2 or 3");
            return true;
        } else {
            return false;
        }
    }
	
	public static boolean checkRepeatError(Table table, int row, int col) {
        if (Table.table[row][col] == 'O' || Table.table[row][col] == 'X') {
            System.out.println("Please choose another Row and Collum!!");
            return true;
        } else {
            return false;
        }
    }
}
