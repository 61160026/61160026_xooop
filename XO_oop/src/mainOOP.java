import java.util.Scanner;

public class mainOOP {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		Table table = new Table();
		Game game = new Game();
		Player player = new Player();
		char turn = 'O';
		int count = 0;
		String again = "Yes";
		
		game.showWelcome();
		for(;;) {
			table.showTable();
			game.showTurn(turn);
			game.Input(table, count, turn);
			count++;
			
			if (player.checkWinRow(table,turn) == true || player.checkWinCol(table, turn) == true 
					|| player.checkWinDiagonalLeft(table, turn) == true || player.checkWinDiagonalRight(table, turn) == true) {
				game.showScoreO();
				game.showScoreX();
				System.out.println("Do you want to play again?");
				again = kb.next();
				if(again.equals("Yes")) {
					count = 0;
					table.createNewTable();
				}else if(again.equals("No")) {
					game.showBye();
					break;
				}
			}else if (player.checkDraw(table, count) == true) {
				game.showScoreO();
				game.showScoreX();
				System.out.println("Do you want to play again?");
				again = kb.next();
				if(again.equals("Yes")) {
					count = 0;
					table.createNewTable();
				}else if(again.equals("No")) {
					game.showBye();
					break;
				}
            }
			turn = game.changeTurn(turn);
		}

	}

}
